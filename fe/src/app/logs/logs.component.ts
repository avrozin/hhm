import { Component, OnInit, OnDestroy } from '@angular/core';
import { merge, Observable, interval, of as observableOf, Subject } from 'rxjs';
import { catchError, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { RobotService } from '../robot.service';


@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  logs: String[] = [];
  lineIndex = 0;

  constructor(private robotService: RobotService) { }

  ngOnInit() {
  	interval(1000).pipe(
  		takeUntil(this.destroy$),
  		switchMap(() => this.robotService.getLogs(this.lineIndex)),
    ).subscribe((resp) => {
       this.logs = this.logs.concat(resp.logs);
       if (this.logs.length > 35) {
         this.logs = this.logs.slice(-35)
       }
       this.lineIndex = resp.line_index + 1;
    });
  }

  ngOnDestroy() {
  	this.destroy$.next(true);
  	this.destroy$.unsubscribe();
  }

}
