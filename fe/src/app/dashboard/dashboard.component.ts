import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { merge, interval, Observable, of as observableOf, Subject } from 'rxjs';
import { catchError, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { RobotService } from '../robot.service';
import { Service } from '../service';
import { FilterService } from '../filter.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  displayedColumns: string[] = [
    'id',
    'pid',
    'name',
    'state',
    'cpu',
    'mem',
    'info',
    'date_last_update',
  ];

  pageSize = 100;
  currPage = 0;
  currSort = 'id';
  currOrder = 'asc';
  services: Service[] = [];
  resultsLength = 0;
  autorefresh$ = interval(5000);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private robotService: RobotService,
              private filterService: FilterService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.sort.sortChange.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => this.paginator.pageIndex = 0);

    merge(this.activatedRoute.queryParams, this.autorefresh$).pipe(
        takeUntil(this.destroy$),
        switchMap((params) => {
          const filter = this.filterService.currentFilter;
          const page = params['page'] ? Number(params['page']) : 0;
          this.currPage = page || filter.page;
          this.currSort = params['sort'] || filter.sort;
          this.currOrder = params['order'] || filter.order;
          this.filterService.update(
            this.currSort, this.currOrder, this.currPage);
          return this.robotService.getCarriers(
            this.currSort, this.currOrder, this.currPage, this.pageSize);
        })
    ).subscribe(resp => {
      this.services = resp.services;
      this.resultsLength = resp.total_count;
    });

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        takeUntil(this.destroy$),
      ).subscribe(() => {
        const sort = this.sort.active || 'id';
        const order = this.sort.direction || 'asc';
        const page = this.paginator.pageIndex || 0;
        this.filterService.update(sort, order, page);
        this.router.navigate(['/dashboard'], {
          queryParams: {
            'sort': sort,
            'order': order,
            'page': page,
          }
        });
      });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
