import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  modalActive = false;
  @Input() large = false;

  constructor() { }

  ngOnInit() {
  }

  open() {
      this.modalActive = true;
  }

  close() {
    this.modalActive = false;
  }

}