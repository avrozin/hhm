import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-trend-icon',
  templateUrl: './trend-icon.component.html',
  styleUrls: ['./trend-icon.component.css']
})
export class TrendIconComponent implements OnInit {

  @Input() trend: string;

  constructor() { }

  ngOnInit() {
  }

  getIcon() {
    if (this.trend === 'up') {
      return 'trending_up';
    }
    return 'trending_down';
  }

  getColor() {
    if (this.trend === 'up') {
      return 'green';
    }
    return 'red';
  }

}
