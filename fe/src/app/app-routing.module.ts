import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { LogsComponent }   from './logs/logs.component';
import { LoginComponent } from './login/login.component';
import { RemotedesctopComponent } from './remotedesctop/remotedesctop.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
  {
  	path: 'dashboard',
  	component: DashboardComponent,
  	canActivate: [AuthGuard],
  },
  {
  	path: 'logs',
  	component: LogsComponent,
  	canActivate: [AuthGuard],
  },
  {
     path: 'login',
     component: LoginComponent,
  },
  
  {
     path: 'remotedesctop',
     component: RemotedesctopComponent,
  },

  // otherwise redirect to home
  { path: '**', redirectTo: '/dashboard' }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
