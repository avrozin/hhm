import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'
import { HttpClient } from '@angular/common/http'
import { timer, fromEvent, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { RobotService } from '../robot.service';
import { saveAs } from 'file-saver/FileSaver';

const UPDATE_TIME = parseInt(localStorage.getItem('UPDATE_TIME'), 10) || 1000;

@Component({
  selector: 'app-root',
  templateUrl: './remotedesctop.component.html',
  styleUrls: ['./remotedesctop.component.css']
})
export class RemotedesctopComponent implements OnInit {
  updateTimer = timer(0, UPDATE_TIME);
  screen: any = '';
  status: any = {};

  telnetOutput = '';
  telnetCmdRunning = false;
  applicationList: string[] = [];
  loadingApplicationList = false;
  notification = '';
  notificationError = true;


  @ViewChild('telnetInput') telnetInput;

  constructor(private http: HttpClient,
              private _sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.updateTimer.subscribe(t => this.updateScreen());
    // this.updateTimer.subscribe(t => this.updateStatuses());
  }

  updateScreen() {
    this.screen = '/api/screen2?' + new Date().getTime();
    // this.http.get('/api/screen', {responseType: 'text'}).subscribe(
    //     (event: string) => {
    //         this.screen = this._sanitizer.bypassSecurityTrustResourceUrl(
    //             'data:image/png;base64,' + event);
    //     });
  }

  updateStatuses() {
    this.http.get('/api/status').subscribe(
        (status) => {
            this.status = status;
        });
  }

  changeStatus(name: string) {
    const req = [name, !this.status[name]];
    this.http.post('/api/change_status', req).subscribe(
        (status) => {
            this.status = status;
        });
  }

  makeAction(actionData: any) {
    const actionName = actionData.action.replace('_', ' ')
    this.http.post('/api/make_action', actionData).subscribe(
      resp => {},
      error => this.showNotification(error.statusText, true),
      () => this.showNotification(`${actionName} complete`));
  }

  onClick(event) {
    const offset = document.getElementById('screen').getBoundingClientRect();
    const x = Math.ceil(event.pageX - offset.left);
    const y = Math.ceil(event.pageY - offset.top);
    const req = [x, y];
    this.http.post('/api/click', req).subscribe();
  }

  makeActionWithFile(inputValue: any, attrs: any): any {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      attrs['file'] = myReader.result;
      attrs['fname'] = file.name;
      this.makeAction(attrs);
    }
    myReader.readAsDataURL(file);
  }

  makeActionWithFileDownload(attrs: any): any {
    this.http.post('/api/make_action', attrs).subscribe(
      resp => {
        var blobContent = atob(resp['file']);
        const blob = new Blob(
          [blobContent], { type: 'application/octet-stream' });
        saveAs(blob, resp['fname']);
      },
      error => this.showNotification(error.statusText, true),
      () => this.showNotification('Download complete'));
  }

  runTelnetCmd(cmd: string) {
    this.telnetCmdRunning = true;
    this.telnetInput.nativeElement.disabled = true;
    this.telnetOutput = ''
    const req = {cmd: cmd};
    this.http.post('/api/run_telnet_cmd', req).subscribe(
        (response) => {
            this.telnetOutput = response['out'];
            this.telnetCmdRunning = false;
            this.telnetInput.nativeElement.disabled = false;
            this.telnetInput.nativeElement.focus();
        });
  }

  getApplicationList() {
    this.http.get('/api/get_app_list').subscribe(
        (response) => {
            this.applicationList = response['apps'];
        });
  }

  showNotification(text: string, error?: boolean) {
    if (error === undefined) {
      error = false;
    }

    this.notificationError = error;
    this.notification = text;

    let notificationTimeout = 2000;
    if (error) notificationTimeout = 10000;
    timer(notificationTimeout).subscribe(t => this.notification = '');

  }

}