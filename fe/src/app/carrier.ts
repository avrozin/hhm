export class Carrier {
  id: number;
  cid: number;
  name: string;
  cps: number;
  sess: number;
  ppm: string;
  ppm_step: string;
  trend: string;
  count_changes_per_day: number;
  count_to_10: number;
  last: number;
  last_5: number;
  last_10: number;
  date_changes: string;
  date_last_update: string;
}