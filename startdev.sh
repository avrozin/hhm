#!/bin/bash

export WEBFOLDER=$(pwd)
export WEBCONFIG=$(pwd)/backend/config.yaml
export WEBDBPATH=$(pwd)/db/services.db3
export LOGSFOLDER=/tmp
export LOGFILE=/tmp/hhm.log

if [[ $1 == '-dev' ]]; then
  export WEBFOLDER=$(pwd)
  export WEBCONFIG=$(pwd)/backend/config.yaml
  export WEBDBPATH=$(pwd)/db/services.db3
  export LOGSFOLDER=/tmp/
  export LOGFILE=/tmp/hhm.log
fi

echo "STEP0"
echo "lets' kill our* processes"
ps -ef  | grep 'hhm/manager.py' | awk '{print $2}' | xargs kill -9
ps -ef  | grep web.py | awk '{print $2}' | xargs kill -9
ps -ef  | grep log_reader_service.py | awk '{print $2}' | xargs kill -9


echo "STEP1"
echo "lets restart log service"
python $WEBFOLDER/backend/hhm/manager.py $WEBCONFIG &

echo "STEP2"
echo "lets restart log service"
python $WEBFOLDER/backend/hhm/log_reader_service.py &


echo "STEP3"
echo "lets restart web service"
python $WEBFOLDER/backend/hhm/web.py 2>&1 | tee $LOGSFOLDER/Web.log

# end of file
