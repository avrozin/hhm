import logging
import logging.config
import os

__author__ = 'krozin@gmail.com'


def get_logger(name):
    CONFIG = os.environ.get('LOGGING_CONF', 'logging.conf')
    LOGFILE = os.environ.get('LOGFILE', '/tmp/{}.log'.format(name))
    if os.path.exists(CONFIG) and os.path.isfile(CONFIG):
        logging.config.fileConfig(CONFIG, disable_existing_loggers=False)
        logger = logging.getLogger(name)
        logger.info('Loaded logger config from: %s', CONFIG)
    else:
        logger = logging.getLogger(name)
        stream_handler = logging.StreamHandler()
        file_handler = logging.FileHandler(LOGFILE, encoding='utf8')
        formatter = logging \
            .Formatter('[%(asctime)s][%(processName)s %(process)-6d]'
                       '[%(levelname)-8s][%(funcName)-20s] '
                       '%(message)s')
        stream_handler.setFormatter(formatter)
        file_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)
        logger.addHandler(file_handler)
        logger.setLevel(logging.DEBUG)
        logger.info('No config found. '
                    'Default logger with DEBUG level is used.')
    return logger


logger = get_logger('hhm')
