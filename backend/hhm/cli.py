#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import json
import random

import click

from hhm import db
from hhm import models


@click.group()
def cli():
    pass


@cli.command()
def createdb():
    models.create_models()


@cli.command()
def createadmin():
    m = models.Users(username='admin', password='123admin321')
    db.session.add(m)
    db.session.commit()


@cli.command()
def create_testdata():
    for i in range(300):
        m = models.Service(
            pid=i,
            name='dummy {}'.format(i),
            state=random.choice([0, 1]),
            cpu=random.randint(0, 100),
            mem=random.randint(0, 100),
            info=json.dumps({i: i * 2}),
            date_last_update=datetime.datetime.now())
        db.session.add(m)
        db.session.commit()


if __name__ == '__main__':
    cli()
