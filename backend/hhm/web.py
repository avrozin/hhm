import base64
import json
import os

import cherrypy
import click
import zmq

from hhm import PACKAGE_DIR
from hhm import db
from hhm import helpers
from hhm import models
from hhm.config import CONF

STATIC_PATH = 'fe/dist/fe'
ROOT_DIR = os.path.join(PACKAGE_DIR, '../../')
SCREEN_PNG_PATH = CONF['backend']['imagepath']

context = zmq.Context()


class Root(object):
    INDEX_PATH = os.path.join(ROOT_DIR, STATIC_PATH, 'index.html')

    @cherrypy.expose
    def default(self, *args, **kargs):
        with open(self.INDEX_PATH) as f:
            return f.read()


class API(object):

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def services(self, sort=None, order=None, page='0', per_page='5'):
        self.check_auth_token()

        session = db.Session()
        services = session.query(models.Service).all()
        count = session.query(models.Service.id).count()
        session.close()
        r = {
            'services': [c.to_dict() for c in services],
            'total_count': count,
        }
        return r

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def service_status(self):
        self.check_auth_token()
        return {'status': helpers.is_service_active('docker')}

    @cherrypy.expose
    def get_logs(self, line_index='0'):
        self.check_auth_token()

        socket = context.socket(zmq.REQ)
        socket.setsockopt(zmq.LINGER, 5000)
        socket.connect('tcp://127.0.0.1:5001')

        print "line_index={}, t={}".format(line_index, type(line_index))

        # TODO: it first time we have NaN value.
        try:
            int_line_index = int(line_index)
        except Exception:
            int_line_index = 0

        request = {'line_index': int(int_line_index)}
        socket.send(json.dumps(request))
        response = socket.recv()
        socket.close()
        return response

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def login(self):
        request = cherrypy.request.json
        username = request['username']
        password = request['password']

        user = models.Users.get_or_none(username=username)
        if not user or not user.verify_password(password):
            raise cherrypy.HTTPError(400, message='Login Failed')

        token = user.generate_auth_token()
        return {'token': token.decode('ascii')}

    def check_auth_token(self):
        auth_header = cherrypy.request.headers.get('Authorization', None)
        if not auth_header:
            raise cherrypy.HTTPError(401, message='Forbidden')

        try:
            bearer, token = auth_header.split(' ')
        except Exception:
            raise cherrypy.HTTPError(401, message='Forbidden')

        user = models.Users.verify_auth_token(token)
        if not user:
            raise cherrypy.HTTPError(401, message='Forbidden')

    @cherrypy.expose
    def screen(self):
        if not os.path.exists(SCREEN_PNG_PATH):
            return ''
        with open(SCREEN_PNG_PATH) as f:
            return base64.encodestring(f.read())

    @cherrypy.expose
    def screen2(self, **vars):
        if not os.path.exists(SCREEN_PNG_PATH):
            return ''
        cherrypy.response.headers['Content-Type'] = 'image/jpg'
        return file(SCREEN_PNG_PATH)


@click.command()
@click.option('--debug/-d', default=False)
def main(debug):
    autoreload = True

    conf = {
        '/': {
            'tools.staticdir.on': True,
            'tools.staticdir.root': ROOT_DIR,
            'tools.staticdir.dir': STATIC_PATH
        },
    }

    root = Root()
    root.api = API()

    cherrypy.config.update({
        'server.socket_port': 8989,
        'log.screen': True,
        'engine.autoreload.on': autoreload,
        'log.access_file': '',
        'log.error_file': '',
    })

    cherrypy.server.socket_host = '0.0.0.0'
    cherrypy.server.socket_port = 8989
    cherrypy.server.thread_pool = 5

    cherrypy.quickstart(root, '/', conf)


if __name__ == '__main__':
    main()
