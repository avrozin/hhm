import datetime
import os
import re
import subprocess
import yaml

from hhm import db
from hhm.logger import logger
from hhm.models import Service

__author__ = 'krozin@gmail.com'


def open_close_session(f):
    def inner(*args, **kargs):
        session = db.Session()
        f(*args, **kargs)
        session.close()
    return inner


def load_config(config):
    # print config
    if isinstance(config, dict):
        return config
    if not (os.path.exists(config) and os.path.isfile(config)):
        return None
    with open(config, 'r') as conffile:
        conf = yaml.load(conffile)
    return conf


def is_service_active(name):
    r = subprocess.call(shlex.split(
        'systemctl is-active --quiet {}.service'.format(name)))
    if r == 0:
        return True
    return False


def service_restart(name):
    r = subprocess.call(shlex.split('sudo service {} restart'.format(name)))
    if r == 0:
        return True
    return False


def get_systemd_pid(name):
    try:
        r = subprocess.check_output(
            shlex.split(r'systemctl -l status {}'.format(name)))
        if not r:
            return
        p_group = re.search(r'Main PID: \d{2,6}', r)
        if not p_group:
            return
        return p_group.group().split(' ')[2]
    except subprocess.CalledProcessError:
        return


def get_cpu_mem_cmd_by_pid(pid):
    try:
        r = subprocess.check_output(
            shlex.split(r'ps -p {} -o %cpu,%mem,cmd'.format(pid)))
        if not r:
            return []
        res = [i for i in r.split(' ') if i][3:]
        cpu = res[0]
        mem = res[1]
        cmd = ' '.join(res[2:])
        return cpu, mem, cmd
    except subprocess.CalledProcessError:
        return []


def update_db(d):
    logger.debug("update_db: start")

    session = db.Session()
    item = session.query(Service).filter_by(name=d.get('name')).first()

    if not item:
        s = Service(
            pid=d.get('pid'),
            name=d.get('name'),
            state=d.get('state'),
            cpu=d.get('cpu'),
            mem=d.get('mem'),
            info=d.get('info'),
            date_last_update=datetime.datetime.now())
        session.add(s)
        session.commit()
        logger.debug("update_db: created")
    else:
        logger.debug("update_db: item exists. item.id={}, pid={}".format(
            item.id, d.get('pid')))
        item.pid = d.get('pid')
        item.cpu = d.get('cpu')
        item.mem = d.get('mem')
        item.state = d.get('state')
        item.date_last_update = datetime.datetime.now()
        session.add(item)
        session.commit()
        logger.debug("update_db: updated")

    session.close()
